package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.model.Book;
import az.ingress.lesson1.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.query.sql.internal.ParameterRecognizerImpl;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;


    @Override
    public int create(BookRequestDto dto) {
        Book book = modelMapper.map(dto, Book.class);
        Book savedBook = bookRepository.save(book);
        return savedBook.getId();
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto dto) {
        Optional<Book> optionalBook = bookRepository.findById(id);

        if (optionalBook.isPresent()) {
            Book bookToUpdate = optionalBook.get();
            modelMapper.map(dto, bookToUpdate);

            Book updatedBook = bookRepository.save(bookToUpdate);
            return modelMapper.map(updatedBook, BookResponseDto.class);
        }else {
            throw new RuntimeException("Book not found with id:" + id);
        }
    }


    @Override
    public void delete(Integer id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            bookRepository.deleteById(id);
        } else {
            throw new RuntimeException("Book Not Found!");
        }
    }

    @Override
    public BookResponseDto get(Integer id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Book not found"));
        return BookResponseDto.builder()
                .author(book.getAuthor())
                .pageCount(book.getPageCount())
                .name(book.getName())
                .build();
    }
}
